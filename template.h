#include <iostream>
using namespace std;

template <typename T> struct list
{
	T key;
	list*next;
};

template <typename T> list*make_list(int n) //������� ��� ������������ ����������������� ������
{
	list<T>*first;   //�������� �� ������ ������� ������
	list<T>*p;  //��������������� ���������
	first = new(list<T>);  //�������� ������ ��� ������ �������
	first->key = n;
	first->next = NULL;
	for (int i = n - 1; i>0; i--)
	{
		p = new(list<T>);
		p->key = i;
		p->next = first;
		first = p;
	}
	return first;
}

template <typename T> void print_list(list<T> *first) //������� ��� ������ ����������������� ������
{
	if (first == NULL)
	{
		cout << "Empty list" << endl;
	}
	else
	{
	list<T>*p = first;
		while (p != NULL)
		{
			cout << p->key << " ";
			p = p->next;
		}
	}
}

template <typename T>list* delete_listelem(list<T> *p, int n)  //�������� �������� � �������� �������
{
	list<T>*cur;//��������������� ���������
	list<T>*q = p; //�������� �������� ��������� ��������� ��������
	int i = 0;  //�������
	while (p != NULL)  //���� ������� ������� �� ����� 0
	{
		if (i == n) //���� ����� �������� �������� ����� ��������, �� ������� ������� ������� 
		{
			cur = p->next; //��������� �������� �������� ��������������� �� ��������� �������
			delete p;  //������� ���������
			p = cur;  //������� ������� ����� ���������� �������� � ������
		}
		i++;
		p = p->next;  //��������� �������
	}
	p = q;  //����������� ���������� ��������

	return p;
}

template <typename T> void insert_listelem(list<T>* p, int c)  //������� ��� ���������� �������� � �������� �������
{
	list<T>* cur = p;  //��������������� ���������
	list <T>*addelem = new (list<T>); //������ ����� ������� ������
	addelem->key = c;  //����������� ��������� ���� �������� ��������
	addelem->next = NULL;  //��������� �� ��������� ������� ����� 0(��������� �������)
	cur = addelem;//��������� ����� �������
}

template <typename T> void insert_file(list<T> *p, int nums)  //������� ��� ������ ������ � ����
{
	FILE *f1;

	int i = 0; //�������
	if ((f1 = fopen("f1.txt", "rb")) == NULL) //���� ���� �� ������, �� ����� ������ ������
	{
		cout << "Error recording file" << endl;
	}

	while (i < nums) //���� i ������,��� ���������� ������� � ������, ���������� ��������� � ���� f1
	{
		fwrite(p, sizeof(list), 1, f1);
		p = p->next;
		i++;
	}
	fclose(f1);
}

template <typename T> void delete_list(list<T> *p)  //������� ��� �������� ������
{
	if (p != NULL)  //���� ��������� �� ����� 0
	{
		p->next;  //��������� �������
		delete p;  //������� ���������
	}
}

template <typename T> void restore_file(struct list<T>, list<T>* p)  //������� ��� �������������� ������ �� �����
{
	FILE *f1;

	int i = 0; //������� 
	if ((f1 = fopen("f1.txt", "wb")) == NULL) //���� ���� �� ������, �� ����� ������ ������
	{
		cout << "Error recording file" << endl;
	}

	while (!feof(f1))  //���� �� ���������� ����
	{
		fread(&p, sizeof(list), 1, f1);  //��������� ������ �� ����� f1
	}
	fclose(f1);
}

template <typename T> void template_main()
{
	int num;
	T del_elem;
	T add_elem;
	cout << "Enter the quantity of nums: " << endl;
	cin >> num;
	p = make_list<T>(num);  //�������� ������� ��� ������������ ����������������� ������
	print_list<T>(p);  //�������� ������� ��� ������ ����������������� ������ �� �����
	cout << "Enter delete element: " << endl;
	cin >> del_elem;
	p = delete_listelem<T>(p, del_elem);  //�������� ������� ��� �������� �������� � �������� �������
	print_list<T>(p);
	cout << "Enter add element: " << endl;
	cin >> add_elem;
	insert_listelem<T>(p, add_elem);  //�������� ������� ��� ���������� �������� � �������� �������

	print_list<T>(p);  //�������� ������� ��� ������ ����������������� ������ �� �����

	insert_file<T>(p, num); //�������� ������� ��� ������ ������ � ����
	delete_list<T>(p); //�������� ������� ��� ����������� ������
}